#====================== BEGIN GPL LICENSE BLOCK ======================
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>
#
#======================= END GPL LICENSE BLOCK ========================

bl_info = {
    "name": "Land generation based on diamond square",
    "description": "Create 3D terrain with relief with using of diamond square algorithm",
    "author": "Nicolas Candia",
    "version": (1, 1),
    "location": "View3D > Tools > Lang Generation",
    "support": "COMMUNITY",
    "category": "Add Mesh"
}

import bpy
from bpy.props import *
import random
import math

# UI Panel for Blender
class LandGenerationPanel(bpy.types.Panel):
    bl_label = "Land Generation Tool"
    bl_idname = "LANDGEN_PANEL"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw(self, context):
        scene = context.scene

        layout = self.layout
        layout.label(text = "Land Generation -")

        row = layout.row()
        row.prop(scene, "LANDGEN_size")
        row = layout.row()
        row.prop(scene, "LANDGEN_variation")
        row = layout.row()
        row.prop(scene, "LANDGEN_roughlevel")

        row = layout.row()
        row.prop(scene, "LANDGEN_minheight")
        row.prop(scene, "LANDGEN_maxheight")

        row = layout.row()
        row.prop(scene, "LANDGEN_realsize")

        layout.label(text = "Height of corners")
        row = layout.row()
        box = row.box()
        box.prop(scene, "LANDGEN_corner1")
        box.prop(scene, "LANDGEN_corner2")
        box.prop(scene, "LANDGEN_corner3")
        box.prop(scene, "LANDGEN_corner4")

        row = layout.row()
        row.operator("landgen.generate_button", text = "Create Land", icon = 'WORLD')

# Blender operator
class LANDGEN_OT_generate_button(bpy.types.Operator):
    bl_idname = "landgen.generate_button"
    bl_label = "Create Land"

    land_generated_id = 0

    def execute(self, context):
        scene = context.scene

        # Use diamond square algorithm to generating height map
        land_data = diamond_square(exp = scene.LANDGEN_size, variation = scene.LANDGEN_variation, corners = [scene.LANDGEN_corner1, scene.LANDGEN_corner2, scene.LANDGEN_corner3, scene.LANDGEN_corner4])

        # Get boundaries
        bound = minmax_dim2(land_data)

        # Rough terrain
        if scene.LANDGEN_roughlevel > 0:
            roughvalue_dim2(land_data, scene.LANDGEN_roughlevel, bound[0], bound[1])
        
        # Creating mesh
        land_size = len(land_data)
        vertices = [[] for i in range(land_size * land_size)]
        faces = []

        print("----------------")
        for i in range(land_size):
            for j in range(land_size):
                real_height = (land_data[i][j] - bound[0]) / (bound[1] - bound[0]) * scene.LANDGEN_maxheight
                vertices[land_size * i + j] = [i * scene.LANDGEN_realsize[0], j * scene.LANDGEN_realsize[1], real_height * scene.LANDGEN_realsize[2]]

        for i in range(land_size - 1):
            for j in range(land_size - 1):
                cursor = land_size * i + j
                faces.append([cursor, cursor + 1, cursor + land_size + 1, cursor + land_size])
        
        self.land_generated_id = self.land_generated_id + 1

        land_mesh = bpy.data.meshes.new("mesh_land_" + str(self.land_generated_id))
        land_mesh.from_pydata(vertices, [], faces)
        ob = bpy.data.objects.new("land_"  + str(self.land_generated_id), land_mesh)

        # Add object in scene
        scene.objects.link(ob)
        scene.objects.active = ob
        ob.select = True

        return {'FINISHED'}

# Blender scene properties
def init_scene_props():
    bpy.types.Scene.LANDGEN_size = IntProperty(name = "Size", default = 2, min = 1)
    bpy.types.Scene.LANDGEN_variation = FloatProperty(name = "Variation", default = 1.0)

    bpy.types.Scene.LANDGEN_maxheight = FloatProperty(name = "Max. height", default = 10.0)
    bpy.types.Scene.LANDGEN_minheight = FloatProperty(name = "Min. height", default = 0.0)

    bpy.types.Scene.LANDGEN_roughlevel = IntProperty(name = "Rough level", default = 0, min = 0)
    bpy.types.Scene.LANDGEN_realsize = FloatVectorProperty(name = "Land size", default = (10.0, 0.0, 10.0))

    bpy.types.Scene.LANDGEN_corner1 = FloatProperty(name = "Corner 1", default = 0.0)
    bpy.types.Scene.LANDGEN_corner2 = FloatProperty(name = "Corner 2", default = 0.0)
    bpy.types.Scene.LANDGEN_corner3 = FloatProperty(name = "Corner 3", default = 0.0)
    bpy.types.Scene.LANDGEN_corner4 = FloatProperty(name = "Corner 4", default = 0.0)

# Perform diamond-square algorithm and return 2 dimensions list
def diamond_square(exp = 3, variation = 5, corners = [0.0, 0.0, 0.0, 0.0]):
    # Size will be a power of two + 1
    size = 1 << (2 if exp < 2 else exp)
    data = [[0.0] * (size + 1) for i in range(size + 1)]

    # Corners initialization
    data[0][0] = corners[0]
    data[0][size] = corners[1]
    data[size][0] = corners[2]
    data[size][size] = corners[3]

    # Run
    cursor = size
    dispalement = 1.0

    while cursor > 1:
        mid = cursor >> 1

        # Square step
        x = mid
        while x < size + 1:
            y = mid
            while y < size + 1:
                square_step(data, x, y, mid, frand() * mid * dispalement)
                y = y + cursor
            x = x + cursor

        # Diamond step
        x = 0
        while x < size:
            shift = mid if x % cursor == 0 else 0
            y = shift
            while y < size:
                diamond_step(data, x, y, mid, frand() * mid * dispalement)
                y = y + cursor
            x = x + mid
        
        cursor = cursor >> 1
        dispalement = dispalement / variation

    return data

# Perform diamond step
def diamond_step(data, x, y, mid, value):
    sum = 0
    n = 0

    if x >= mid:
        sum = sum + data[x - mid][y]
        n = n + 1
    if x + mid < len(data):
        sum = sum + data[x + mid][y]
        n = n + 1
    if y >= mid:
        sum = sum + data[x][y - mid]
        n = n + 1
    if y + mid < len(data):
        sum = sum + data[x][y + mid]
        n = n + 1

    data[x][y] = sum / n + value

# Perform square step
def square_step(data, x, y, size, value):
    data[x][y] = (data[x - size][y - size] + data[x + size][y + size] + data[x - size][y + size] + data[ x + size][y - size]) / 4.0 + value

# Return random value between -1.0 and 1.0
def frand():
    return random.random() if random.random() > 0.5 else -random.random()

# Return maximun and minimun value of 2 dimension list
def minmax_dim2(data):
    result = [float("inf"), float("-inf")]

    for row in data:
        for c in row:
            if c > result[1]:
                result[1] = c
            if c < result[0]:
                result[0] = c

    return result

# Rough value in 2 dimension list
def roughvalue_dim2(data, num_level, min, max):
    level_size = (max - min) / num_level

    for row in data:
        for c in row:
            c = math.ceil(math.floor((c - min) / level_size) * level_size + min)

# Run
def register():
    bpy.utils.register_module(__name__)
    init_scene_props()
    
def unregister():
    bpy.utils.unregister_module(__name__)
    
if __name__ == "__main__":
    register()